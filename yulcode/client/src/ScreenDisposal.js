import React, { Component } from 'react';
import './ScreenDisposal.css';

export default class ScreenDisposal extends Component {
  render() {
    return (
      <div className="screen-disposal">
        { this.props.children }
      </div>
    )
  }
}
