import React from 'react';
import './App.css';
import softwareBar from './softwareBar';
import ScreenDisposal from './ScreenDisposal';
import Navigation from './Navigation';

function App() {
  return (
    <div>
      <div>
        <Navigation></Navigation>
        <div className="App-height col-12">
          <div className="col-2 red component-height">
            <softwareBar></softwareBar>
          </div>
          <div className = "col-10 blue component-height">
            <ScreenDisposal></ScreenDisposal>
          </div>
        </div>
      </div>
    </div>
    
  );
}

export default App;
